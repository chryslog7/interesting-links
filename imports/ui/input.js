import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Categories } from '../api/categories.js';
import './category.js';
import './link.js';
import './input.html';
import { uiCallback } from './ui-callback';

Template.input.helpers({
    categoriesNumber() {
        return Categories.find({owner: FlowRouter.getParam('username')}).count();
    },
    categories() {
        return Categories.find({owner: FlowRouter.getParam('username')}, {sort: {createdAt: -1}});
    },
    isYourLinksList() {
        return Meteor.user() && Meteor.user().username == FlowRouter.getParam('username');
    }
});

Template.input.events({
    'submit .new-category'(event) {
        event.preventDefault();

        const target = event.target;
        const name = target.name.value;

        //input verification

        if(name == "")
            uiCallback(`You didn't type anything dude!`);
        else if(Categories.findOne({name: name}))
            uiCallback(`this category already exists`);
        //insert
        else{
            Meteor.call('input.new-category', name);

            //clear form
            target.name.value = '';

            // Close modale
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    },
    'submit .new-link'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;

        const link = target.link.value;
        const description = target.description.value;
        const categoryId = target.category.value;

        let verification = true;
        //input verification
        if(description == "" || link == ""){
            verification = false;
            uiCallback(`All the fields have to be filled dude`);
        }
        let linksList = Categories.findOne({_id: categoryId}).links
        for(let i=0 ; i<linksList.length; i++)
            if(linksList[i].url == link){
                verification = false;
                uiCallback(`${linksList[i].owner} already added this link in this category under the name of "${linksList[i].description}"`);
                break;
            }

        //insert
        if(verification){
            Meteor.call('input.new-link', link, description, categoryId);

            // Clear form
            target.link.value = '';
            target.description.value = '';

            // Close modale
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    },
});
