import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

import './users-list.html';

Template.usersList.onCreated(() => {
    Meteor.subscribe('usersList');
});

Template.usersList.helpers({
    users(){
        return Meteor.users.find({});
    }
});
