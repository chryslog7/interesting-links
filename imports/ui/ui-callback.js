Platform = {
  isIOS: function () {
    return (!!navigator.userAgent.match(/iPad/i) || !!navigator.userAgent.match(/iPhone/i) || !!navigator.userAgent.match(/iPod/i))
           || Session.get('platformOverride') === 'iOS';
  },

  isAndroid: function () {
    return navigator.userAgent.indexOf('Android') > 0
           || Session.get('platformOverride') === 'Android';
  }
};

export const uiCallback = (message) => {
    if ((Meteor.isCordova && Platform.isAndroid())) {
        window.plugins.toast.showShortCenter(message);
    }
    else {
        alert(message);
    }
};
