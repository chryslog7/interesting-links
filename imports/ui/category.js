import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Categories } from '../api/categories.js';
import './category.html';

Template.category.helpers({
    is_owner() {
        if(Meteor.user())
            return (this.owner === Meteor.user().username);
        else {
            return false;
        }
    }
});

Template.category.events({
    'click .delete'() {
        Meteor.call("category.remove", this._id, this.owner);
    },
});
