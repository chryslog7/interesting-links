import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import Clipboard from 'clipboard';

import { Categories } from '../api/categories.js';
import './input.js';
import './app-theme.js';
import './index.html';

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY'
});

Template.index.onCreated(() => {
    Meteor.subscribe('categories');
});

Template.index.helpers({
    username() {
        return FlowRouter.getParam('username');
    },
    is_owner() {
        if(Meteor.user())
            return (FlowRouter.getParam('username') === Meteor.user().username);
        else {
            return false;
        }
    },
    categories() {
        let cats = Categories.find(
            {owner: FlowRouter.getParam('username')},
            {sort: {createdAt: -1}}
        );
        return cats.count() > 0 ? cats : false;
    }
});

if (Meteor.isClient) {
    Template.index.onRendered(function() {
        var clipboard = new Clipboard('.btn-copy-link');
    });
}
