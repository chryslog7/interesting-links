import { Categories } from '../api/categories.js';
import './index.js';
import './users-list.js';
import './app.html';

Template.app.helpers({
    theme() {
        return `theme-${Meteor._localStorage.getItem('app-theme')}`;
    },
    activeNavLink(navLink) {
        const active =
            navLink === FlowRouter.getParam('username')
            ||
            navLink == 'undefined'
            && ActiveRoute.name('usersList');

        return active && "activeNavLink";
    },
    inputAvailable() {
        /*
        Make input available only if the user is logged and
        is on his own list or the list of someone else with at least
        one category to add a link into
        */
        let username = FlowRouter.getParam('username');
        return Meteor.user()
        && (
            Meteor.user().username == username
            ||
            FlowRouter.getRouteName() == 'index.user'
            && Categories.find({owner: username}).count()
        );
    }
});

Template.app.events({
    'click': function(evt) {
        if (evt.target.id === "addLink") {
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    }
});
